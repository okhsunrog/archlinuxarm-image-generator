#!/bin/bash

NAME="ArchLinuxARM-aarch64-latest"
IMAGE="ArchLinuxARM-odroid-n2-x"

platform_variables() {
    echo "blank"
}

platform_pre_chroot() {
    echo "Platform pre-chroot..."
}

platform_chroot_setup() {
    echo "Platform chroot-setup..."
    alarm_pacman uboot-odroid-n2plus
    /usr/bin/mkimage -n "Ramdisk Image" -A arm64 -O linux -T ramdisk -C none -d /boot/initramfs-linux.img /boot/initramfs-linux.uimg
    cp /mods/boot/boot-logo.alarm.bmp.gz /boot/boot-logo.bmp.gz
}

platform_chroot_setup_exit() {
    echo "Platform chroot-setup-exit..."
    # Install at last since this causes issues
}

platform_post_chroot() {
    echo "Platform post-chroot..."

    echo "Setting boot.ini UUID"
    local loopname=$(echo "${LOOP}" | sed "s/\/dev\///g")

    local uuidboot=$(lsblk -o uuid,name | grep ${loopname}p1 | awk "{print \$1}")
    local uuidroot=$(lsblk -o uuid,name | grep ${loopname}p2 | awk "{print \$1}")

    sudo sed -i "s/root=\/dev\/mmcblk\${devno}p2/root=UUID=${uuidroot}/g" \
        root/boot/boot.ini

    sudo sed -i "s/setenv bootlabel \"ArchLinux\"/setenv bootlabel \"ArchLinux ${ENV_NAME}\"/g" \
        root/boot/boot.ini

    sudo echo "UUID=${uuidboot}  /boot  vfat  defaults,noatime,discard  0  0" \
        | sudo tee --append root/etc/fstab

#    echo "Flashing U-Boot..."
    sudo dd if=root/boot/u-boot.bin of=${LOOP} conv=fsync,notrunc bs=512 seek=1
    sync
}
