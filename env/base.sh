#!/bin/bash

alarm_install_package() {
    package=$(ls /mods/packages/$1*.pkg.tar.* | sort | tail -n1)
    yes | pacman -U "$package"
}

alarm_pacman() {
    yes | pacman -S --needed --noconfirm $@
}

# Include environment hooks
if [ -e /env.sh ]; then
    source /env.sh
fi

if [ -e /platform.sh ]; then
    source /platform.sh
fi

#
# Try to set locale before installing packages.
#
sed -i 's/#en_US\.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
locale-gen
localectl set-locale en_US.UTF-8
echo "mira" > /etc/hostname
#
# PACMAN SETUP
#
pacman-key --init
pacman-key --populate archlinuxarm

#
# Add custom repo for some extra packages
#
alarm_install_package archlinuxdroid-repo

#
# Sync pacman databases
#
echo "ALL_config=\"/etc/mkinitcpio.conf\"
ALL_kver=\"/boot/Image\"
PRESETS=('default')
default_image=\"/boot/initramfs-linux.img\"" > /etc/mkinitcpio.d/linux-aarch64.preset
rm /boot/init*
pacman -Suy --noconfirm

# Devel
# alarm_pacman base-devel cmake git arch-install-scripts

# Network
alarm_pacman openssh

sed -i "s/#PermitRootLogin prohibit-password/PermitRootLogin yes/g" /etc/ssh/sshd_config

# System
alarm_pacman cpupower

#
# INSTALL CUSTOM PACKAGES
#
#alarm_install_package yay-bin


#
# Initial alarm user account setup
#
#usermod -G audio,tty,video,input,wheel,network,realtime -a alarm
#cp /mods/etc/sudoers.d/wheel /etc/sudoers.d/
#cp /mods/home/alarm/.makepkg.conf /home/alarm/
#chown -R alarm:alarm /home/alarm

userdel -r -f alarm

#
# SETUP HOOKS
#
if type "platform_chroot_setup" 1>/dev/null ; then
    echo "Executing platform chroot setup hook..."
    platform_chroot_setup
fi

if type "env_chroot_setup" 1>/dev/null ; then
    echo "Executing environment chroot setup hook..."
    env_chroot_setup
fi


#
# TWEAKS
#

# Fix domain resolution issue
cp /mods/etc/systemd/resolved.conf /etc/systemd/

# Add initial setup script
cp /mods/etc/systemd/system/initial-setup.service /etc/systemd/system/
cp /mods/usr/bin/initial-img-setup /usr/bin/


#
# CUSTOMIZATIONS
#
cp /mods/etc/fstab /etc/
cp /mods/etc/sysctl.d/general.conf /etc/sysctl.d/general.conf
cp /mods/etc/default/cpupower /etc/default/
#cp /mods/etc/vconsole.conf /etc/

#chown -R alarm:alarm /home/alarm


#
# SYSTEM SERVICES
#
systemctl enable sshd
systemctl enable cpupower
systemctl enable systemd-resolved
systemctl enable systemd-timesyncd
systemctl enable initial-setup


#
# Exit HOOKS
#
if type "platform_chroot_setup_exit" 1>/dev/null ; then
    echo "Executing platform chroot setup exit hook..."
    platform_chroot_setup_exit
fi

exit
